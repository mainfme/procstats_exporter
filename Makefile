.PHONY: build run watch push build_package package

NAME := procstats_exporter
VERSION := 0.2
ITERATION := 1.pltrm.el7.centos
RPM_VERSION := ${VERSION}-${ITERATION}
ARCH := x86_64
DOCKER_REGISTRY := docker.pltrm.net
DOCKER_IMAGE := ${DOCKER_REGISTRY}/${NAME}:latest
DOCKER_IMAGE_LATEST := ${DOCKER_REGISTRY}/${NAME}:${VERSION}
DOCKER_BUILD := ${DOCKER_REGISTRY}/${NAME}_build:latest

### PACKAGE RELATED ###

build:
	docker build -t $(DOCKER_IMAGE_LATEST) -t $(DOCKER_IMAGE) .

push:
	docker push $(DOCKER_IMAGE)
	docker push $(DOCKER_IMAGE_LATEST)

build_package: clean
	docker build -t $(DOCKER_BUILD) dist/

package: build_package
	docker cp $(shell docker create $(DOCKER_BUILD)):/pkg/$(NAME)-$(RPM_VERSION).$(ARCH).rpm dist/
	docker rmi -f $(DOCKER_BUILD)

clean:
	docker rmi -f $(DOCKER_BUILD)
	rm -f dist/*.rpm

### SOFTWARE RELATED ###

run:
	docker run -e PROCSTATS_EXPORTER_PROCS="cat foo bar" -p 8080:8080 -it $(DOCKER_IMAGE)

watch:
	watch -n 1 "curl -s localhost:8080/metrics | grep procstats_exporter"
