# Build our binary in a separate container
FROM golang:alpine AS builder

ENV GOPATH "/go"
ENV SLUG "src/github.com/pltrm/procstats_exporter"
ENV NAME "procstats_exporter"

# Install dep
RUN apk --no-cache add git binutils
RUN wget -O /usr/local/bin/dep https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 && chmod +x /usr/local/bin/dep

# Copy source code
RUN mkdir -p ${GOPATH}/${SLUG}
WORKDIR ${GOPATH}/${SLUG}
COPY *.go Gopkg.toml ./

# Install dependencies and build
RUN go get .
RUN go build -tags netgo -v
RUN strip ${NAME}

# Copy our binary from builder container and run it
FROM alpine:latest
WORKDIR /root
# Somehow using variables here leads to copying the whole working directory from the builder
# COPY --from=builder ${GOPATH}/${SLUG}/${NAME} .
COPY --from=builder /go/src/github.com/pltrm/procstats_exporter/procstats_exporter /root
EXPOSE 8080

CMD ["/root/procstats_exporter"]
