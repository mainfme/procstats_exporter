package procstats_exporter

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/coreos/go-log/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	ramUsageMetric = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "procstats_exporter_process_resident_memory_bytes",
			Help: "Memory usage by process",
		},
		[]string{
			"process", "pid",
		},
	)
	cpuUsageMetric = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "procstats_exporter_process_cpu_usage",
			Help: "CPU usage by process",
		},
		[]string{
			"process", "pid",
		},
	)
	fdUsedMetric = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "procstats_exporter_process_fd_used",
			Help: "proccess used fd",
		},
		[]string{
			"process", "pid",
		},
	)
	fdSoftLimitMetric = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "procstats_exporter_process_fd_soft_limit",
			Help: "Process fd soft limit",
		},
		[]string{
			"process", "pid",
		},
	)
	fdHardLimitMetric = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "procstats_exporter_process_fd_hard_limit",
			Help: "Process fd hard limit",
		},
		[]string{
			"process", "pid",
		},
	)
	verbose = kingpin.Flag("verbose", "Enable debug information").Default("false").Short('v').Bool()
	port    = kingpin.Flag("port", "Listening port").Default("8080").Short('p').String()
	procs   = kingpin.Arg("procs", "Procceses to monitoring").Required().String()
	logger  *log.Logger
	procMap = make(map[string]*processInfo)
)

type processInfo struct {
	name        string
	pid         string
	memUsage    float64
	cpuUsage    float64
	usedFd      float64
	hardFdLimit float64
	softFdLimit float64
}

func checkProcs() {
	for i := range procMap {
		_, err := os.Open(fmt.Sprintf("/proc/%s/status", procMap[i].pid))
		if err != nil {
			logger.Error(err)
			ramUsageMetric.Delete(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid})
			cpuUsageMetric.Delete(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid})
			fdUsedMetric.Delete(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid})
			fdHardLimitMetric.Delete(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid})
			fdSoftLimitMetric.Delete(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid})
			logger.Debugf("%s (pid: %s) metrics removed", procMap[i].name, procMap[i].pid)
			delete(procMap, i)
		}
	}
}

func getPid() {
	logger.Debugf("Command line arguments: %s", *procs)
	procList := strings.Split(*procs, ",")
	logger.Debugf("List of procecces: %s", procList)
	for _, proc := range procList {
		out, err := exec.Command("/bin/pidof", proc).Output()
		if err == nil {
			pids := strings.Split(string(out), " ")
			for _, pid := range pids {
				pid = strings.Trim(pid, " \n")
				procinf := processInfo{}
				procinf.name = proc
				procinf.pid = pid
				procMap[pid] = &procinf
			}
		}
	}
}

func getRamUsage() {
	for i := range procMap {
		file, err := os.Open(fmt.Sprintf("/proc/%s/status", procMap[i].pid))
		if err != nil {
			logger.Error(err)
		} else {
			scanner := bufio.NewScanner(file)
			for scanner.Scan() {
				line := strings.Fields(scanner.Text())
				if strings.HasPrefix(line[0], "VmRSS") {
					memUsageKB, err := strconv.ParseFloat(line[1], 64)
					if err != nil {
						logger.Error(err)
					}
					memUsage := memUsageKB * 1024
					procMap[i].memUsage = memUsage

					logger.Debugf("%s (pid %s) mem usage: %f(added)", procMap[i].name, procMap[i].pid, memUsage)

				}
			}
		}
	}
}

func getCpuUsage() {
	var cpuTime float64
	var procTime float64
	var cpuUsage float64
	rxp := regexp.MustCompile(`cpu* \d* \d* \d* \d* \d* \d* \d* \d* \d*`)
	for i := range procMap {
		cpuStat, err := os.Open("/proc/stat")
		if err != nil {
			logger.Error(err)
		} else {
			scannerCpuStat := bufio.NewScanner(cpuStat)
			for scannerCpuStat.Scan() {
				line := strings.Fields(scannerCpuStat.Text())
				if rxp.MatchString(scannerCpuStat.Text()) {
					for j := 1; j < 11; j++ {
						cpuStatTime, err := strconv.ParseFloat(line[j], 64)
						if err != nil {
							logger.Error(err)
						}
						cpuTime += cpuStatTime
					}
					logger.Debugf("All cpu time: %f", cpuTime)
				}
			}
		}
		procStat, err := os.Open(fmt.Sprintf("/proc/%s/stat", procMap[i].pid))
		if err != nil {
			logger.Error(err)
		} else {
			scannerProcStat := bufio.NewScanner(procStat)
			for scannerProcStat.Scan() {
				line := strings.Fields(scannerProcStat.Text())

				procStatTime, err := strconv.ParseFloat(line[15], 64) //проверить ту ли переменную беру
				if err != nil {
					logger.Error(err)
				}
				procTime = procStatTime
			}
			logger.Debugf("%s (pid %s) cpu time: %f", procMap[i].name, procMap[i].pid, procTime)
			cpuUsage = procTime / cpuTime
			cpuTime = 0
			procMap[i].cpuUsage = cpuUsage
			logger.Debugf("%s cpu (pid %s) usage %f (added)", procMap[i].name, procMap[i].pid, cpuUsage)
		}
	}

}

func getFdUsage() {
	var usedFd float64
	var hardMaxFd float64
	var softMaxFd float64

	for i := range procMap {
		rxp := regexp.MustCompile(`Max open files *\d* *\d* *\w* `)
		files, err := ioutil.ReadDir(fmt.Sprintf("/proc/%s/fd", procMap[i].pid))
		if err != nil {
			logger.Error(err)
		} else {
			for range files {
				usedFd++
			}
		}
		procMap[i].usedFd = usedFd
		logger.Debugf("%s (pid %s) use %f fd(added)", procMap[i].name, procMap[i].pid, procMap[i].usedFd)
		limits, err := os.Open(fmt.Sprintf("/proc/%s/limits", procMap[i].pid))
		if err != nil {
			logger.Error(err)
		} else {
			scannerFd := bufio.NewScanner(limits)
			for scannerFd.Scan() {
				line := strings.Fields(scannerFd.Text())
				if rxp.MatchString(scannerFd.Text()) {
					hardMaxFd, err = strconv.ParseFloat(line[4], 64)
					if err != nil {
						logger.Error(err)
					}
					procMap[i].hardFdLimit = hardMaxFd
					logger.Debugf("%s (pid %s) hard max fd(added): %f", procMap[i].name, procMap[i].pid, procMap[i].hardFdLimit)
					softMaxFd, err = strconv.ParseFloat(line[3], 64)
					if err != nil {
						logger.Error(err)
					}
					procMap[i].softFdLimit = softMaxFd
					logger.Debugf("%s (pid %s) soft max fd(added): %f", procMap[i].name, procMap[i].pid, procMap[i].softFdLimit)
				}
			}
		}
	}
}

func updateMetrics() {
	for {
		checkProcs()
		getPid()
		getRamUsage()
		getCpuUsage()
		getFdUsage()
		for i := range procMap {
			ramUsageMetric.With(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid}).Set(procMap[i].memUsage)
			cpuUsageMetric.With(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid}).Set(procMap[i].cpuUsage)
			fdUsedMetric.With(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid}).Set(procMap[i].usedFd)
			fdHardLimitMetric.With(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid}).Set(procMap[i].hardFdLimit)
			fdSoftLimitMetric.With(prometheus.Labels{"process": procMap[i].name, "pid": procMap[i].pid}).Set(procMap[i].softFdLimit)
		}
		time.Sleep(15 * time.Second)
	}
}

func initLogger(verbose bool) *log.Logger {
	if verbose != false {
		logger := log.NewSimple(log.PriorityFilter(log.PriDebug, log.WriterSink(os.Stdout, log.BasicFormat, log.BasicFields)))
		return logger
	} else {
		logger := log.NewSimple(log.PriorityFilter(log.PriInfo, log.WriterSink(os.Stdout, log.BasicFormat, log.BasicFields)))
		return logger
	}
}

func exporter() {
	http.Handle("/metrics", promhttp.Handler())
	logger.Infof("Handling /metrics on %s", fmt.Sprintf(":%s", *port))
	logger.Fatalln(http.ListenAndServe(fmt.Sprintf(":%s", *port), nil))

}

func main() {
	kingpin.CommandLine.HelpFlag.Short('h')
	kingpin.Parse()
	logger = initLogger(*verbose)
	prometheus.MustRegister(ramUsageMetric)
	prometheus.MustRegister(cpuUsageMetric)
	prometheus.MustRegister(fdUsedMetric)
	prometheus.MustRegister(fdHardLimitMetric)
	prometheus.MustRegister(fdSoftLimitMetric)
	go exporter()
	go updateMetrics()
	forever := make(chan bool)
	<-forever
}
