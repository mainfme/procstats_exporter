
usage:` ./procstats_exporter [<flags>] <procs>`

Flags:
```
  -h, --help         Show context-sensitive help (also try --help-long and --help-man).
  -v, --verbose      Enable debug information
  -p, --port="8080"  Listening port
```

Args:
```
  <procs>  Procceses to monitoring (separate with comma)
```

Statically and stripped building (need to download dependencies first `go get .`):
```
 CGO_ENABLED=0 GOOS=darwin go build -a -ldflags '-extldflags "-static" -s -w' main.go
```